package ru.ekfedorov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractListener;
import ru.ekfedorov.tm.util.NumberUtil;

@Component
public final class InfoListener extends AbstractListener {

    @NotNull
    @Override
    public String commandArg() {
        return "-i";
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Show system info.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "info";
    }

    @Override
    @EventListener(condition = "@infoListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        System.out.println("[SYSTEM INFO]");
        final int processors = Runtime.getRuntime().availableProcessors();
        @NotNull final String processorMsg = "Available processors: " + processors;
        @NotNull String freeMemoryMsg = "Free memory: ";
        final long freeMemory = Runtime.getRuntime().freeMemory();
        freeMemoryMsg += NumberUtil.format(freeMemory);
        final long maxMemory = Runtime.getRuntime().maxMemory();
        @NotNull final String maxMemoryFormat = NumberUtil.format(maxMemory);
        @NotNull String maxMemoryMsg = "Maximum memory: ";
        maxMemoryMsg += maxMemoryValue(maxMemory, maxMemoryFormat);
        @NotNull String totalMemoryMsg = "Total memory available to JVM: ";
        final long totalMemory = Runtime.getRuntime().totalMemory();
        totalMemoryMsg += NumberUtil.format(totalMemory);
        @NotNull String usedMemoryMsg = "Used memory by JVM: ";
        final long usedMemory = totalMemory - freeMemory;
        usedMemoryMsg += NumberUtil.format(usedMemory);
        System.out.println(processorMsg);
        System.out.println(freeMemoryMsg);
        System.out.println(maxMemoryMsg);
        System.out.println(totalMemoryMsg);
        System.out.println(usedMemoryMsg);
    }

    @NotNull
    private String maxMemoryValue(final long maxMemory, @NotNull final String maxMemoryFormat) {
        return (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
    }

}
