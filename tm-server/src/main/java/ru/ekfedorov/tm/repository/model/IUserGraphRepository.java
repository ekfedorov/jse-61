package ru.ekfedorov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.ekfedorov.tm.api.repository.IGraphRepository;
import ru.ekfedorov.tm.model.UserGraph;

import java.util.Optional;

public interface IUserGraphRepository extends IGraphRepository<UserGraph> {

    @NotNull
    Optional<UserGraph> findByLogin(@NotNull String login);

    void removeByLogin(@NotNull String login);

}
